// Scene and Camera
let scene = document.getElementById('scene');
let rig = document.querySelector('#cameraRig');
let head = document.querySelector('#head');
let avatar = document.getElementById('avatar');
// Interface
let interface = document.getElementById('interface');
// Graphs
let mediaOpen = document.getElementById('median-open-time');
let percentOpen = document.getElementById('80-percent-open-time');
let stats = document.getElementById('status');
let prByOrg = document.getElementById('pull-requests-by-organization');
let submitters = document.getElementById('submitters');
let submitter = document.getElementById('submitter');
let pr = document.getElementById('pull-requests');
let org = document.getElementById('organizations');
let projects = document.getElementById('projects');
let repos = document.getElementById('repositories');

// Interface
function interfaceListener(interface){
    interface.addEventListener('click', function (event) {
        console.log('CLICK')
        // for each graph
        if (!NAF.utils.isMine(mediaOpen)) {
            NAF.utils.takeOwnership(mediaOpen)
        };

        if (!NAF.utils.isMine(percentOpen)) {
            NAF.utils.takeOwnership(percentOpen)
        };

        if (!NAF.utils.isMine(stats)) {
            NAF.utils.takeOwnership(stats)
        };

        if (!NAF.utils.isMine(prByOrg)) {
            NAF.utils.takeOwnership(prByOrg)
        };

        if (!NAF.utils.isMine(submitters)) {
            NAF.utils.takeOwnership(submitters)
        };

        if (!NAF.utils.isMine(submitter)) {
            NAF.utils.takeOwnership(submitter)
        };

        if (!NAF.utils.isMine(pr)) {
            NAF.utils.takeOwnership(pr)
        };

        if (!NAF.utils.isMine(org)) {
            NAF.utils.takeOwnership(org)
        };

        if (!NAF.utils.isMine(projects)) {
            NAF.utils.takeOwnership(projects)
        };

        if (!NAF.utils.isMine(repos)) {
            NAF.utils.takeOwnership(repos)
        };
    });
}
interfaceListener(interface);

/* SET AVATAR FROM QUERY STRING */
let y = 0;
let chosenAvatar = getValueFromQueryString('avatar');
if (chosenAvatar) {
    if (chosenAvatar == 'astro') {
        avatar.setAttribute("gltf-model", "#astro");
        avatar.setAttribute('scale', '0.0015 0.0015 0.0015');
        avatar.setAttribute('position', '0 -0.5 0')
    } else if (chosenAvatar == 'bot') {
        avatar.setAttribute("gltf-model", "#bot");
        avatar.setAttribute('scale', '0.1 0.1 0.1');
        avatar.setAttribute('rotation', '0 90 0');
        avatar.setAttribute('position', '0 0.1 0');
    } else if (chosenAvatar == 'charmander') {
        avatar.setAttribute("gltf-model", "#charmander");
        avatar.setAttribute('scale', '0.025 0.025 0.025');
        avatar.setAttribute('position', '0 0.5 0');
    } else if (chosenAvatar == 'dinosaur') {
        avatar.setAttribute("gltf-model", "#dinosaur");
        avatar.setAttribute('scale', '0.25 0.25 0.25');
        avatar.setAttribute('position', '0 0.3 0');
    } else if (chosenAvatar == 'dwarf') {
        avatar.setAttribute("gltf-model", "#dwarf");
        avatar.setAttribute('scale', '0.4 0.4 0.4');
        avatar.setAttribute('position', '0 -0.5 0')
    } else if (chosenAvatar == 'nigiri') {
        avatar.setAttribute("gltf-model", "#nigiri");
        avatar.setAttribute('scale', '0.015 0.015 0.015');
        avatar.setAttribute('position', '0 -0.5 0')
    } else if (chosenAvatar == 'owl_sleep') {
        avatar.setAttribute("gltf-model", "#owl_sleep");
        avatar.setAttribute('scale', '0.4 0.4 0.4');
        avatar.setAttribute('position', '0 -0.5 0')
    } else if (chosenAvatar == 'penguin') {
        avatar.setAttribute("gltf-model", "#penguin");
        avatar.setAttribute('scale', '1 1 1');
        avatar.setAttribute('position', '0 1 0');
    } else if (chosenAvatar == 'pidgeon') {
        avatar.setAttribute("gltf-model", "#pidgeon");
        avatar.setAttribute('scale', '1.4 1.4 1.4');
        avatar.setAttribute('rotation', '0 0 0');
        avatar.setAttribute('position', '0 -0.5 0')
    } else if (chosenAvatar == 'rubberduck') {
        avatar.setAttribute("gltf-model", "#rubberduck");
        avatar.setAttribute('scale', '1.5 1.5 1.5');
        avatar.setAttribute('rotation', '0 90 0');
        avatar.setAttribute('position', '0 -0.5 0')
    } else if (chosenAvatar == 'shiba') {
        avatar.setAttribute("gltf-model", "#shiba");
        avatar.setAttribute('scale', '0.75 0.75 0.75');
        avatar.setAttribute('position', '0 1 0');
    }
}

/* SET SERVER URL FROM QUERY STRING */
let serverURL = getValueFromQueryString('serverURL');
console.log("Empty server URL, networked-scene: ", scene.getAttribute('networked-scene'));
if (serverURL) {
    scene.setAttribute('networked-scene', 'serverURL', serverURL);
    scene.setAttribute('networked-scene', 'audio', 'true');
}

// VR started
document.addEventListener('controllerconnected', (event) => {
    mov.setAttribute('position', "3 -4.5 4");
    if (chosenAvatar == 'astro') {
        avatar.setAttribute('position', '0 -1.25 0')
    } else if (chosenAvatar == 'bot') {
        avatar.setAttribute('position', '0 -0.65 0');
    } else if (chosenAvatar == 'charmander') {
        avatar.setAttribute('position', '0 -0.25 0');
    } else if (chosenAvatar == 'dinosaur') {
        avatar.setAttribute('position', '0 -0.45 0');
    } else if (chosenAvatar == 'dwarf') {
        avatar.setAttribute('position', '0 -1.25 0')
    } else if (chosenAvatar == 'nigiri') {
        avatar.setAttribute('position', '0 -1.25 0')
    } else if (chosenAvatar == 'owl_sleep') {
        avatar.setAttribute('position', '0 -1.25 0')
    } else if (chosenAvatar == 'penguin') {
        avatar.setAttribute('position', '0 0.25 0');
    } else if (chosenAvatar == 'pidgeon') {
        avatar.setAttribute('position', '0 -1.25 0')
    } else if (chosenAvatar == 'rubberduck') {
        avatar.setAttribute('position', '0 -1.25 0')
    } else if (chosenAvatar == 'shiba') {
        avatar.setAttribute('position', '0 0.25 0');
    }
});



scene.addEventListener('child-attached', function (event) {
    if (event.detail.el.id === 'babia-menu-hand') {
        let uiVR = document.querySelector('#babia-menu-hand')
        interfaceListener(uiVR);
    }
});


// Get Server
function getValueFromQueryString(string) {
    string = string.replace(/[\[\]]/g, '\\$&');
    var regex = new RegExp('[?&]' + string + '(=([^&#]*)|&|#|$)'),
        results = regex.exec(window.location.href);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, ' '));
}

                                
// Schemas with components and attributes for syncronization
NAF.schemas.add({
    template: '#rig-template',
    components: [
        'position',
        'rotation'
    ]
});
NAF.schemas.add({
    template: '#camera-template',
    components: [
        'position',
        'rotation'
    ]
});

NAF.schemas.add({
    template: '#avatar-template',
    components: [
        'position',
        'rotation',
        'scale',
        'gltf-model'
    ]
});
NAF.schemas.add({
    template: '#bars-template',
    components: [
        'position',
        'rotation',
        'scale',
        'babia-bars',
        'babia-queryes'
    ]
});
NAF.schemas.add({
    template: '#pie-template',
    components: [
        'position',
        'rotation',
        'scale',
        'babia-pie',
        'babia-queryes'
    ]
});
NAF.schemas.add({
    template: '#doughnut-template',
    components: [
        'position',
        'rotation',
        'scale',
        'babia-doughnut',
        'babia-queryes'
    ]
});
NAF.schemas.add({
    template: '#barsmap-template',
    components: [
        'position',
        'rotation',
        'scale',
        'babia-barsmap',
        'babia-queryes'
    ]
});
